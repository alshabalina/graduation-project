from projectdb import Admin

from flask import Flask, request


app = Flask(__name__)


@app.route('/product/', methods=['POST'])
def product():
    admin = Admin()
    admin.add_product(request.get_json()["name"])
    return "Added", 201 

   
@app.route('/wine/', methods=['POST'])
def wine():
    admin = Admin()
    admin.add_wine(request.get_json()["name"])
    return "Added", 201


@app.route('/spice/', methods=['POST'])
def spice():
    admin = Admin()
    admin.add_spice(request.get_json()["name"])
    return "Added", 201


@app.route('/prodwine/', methods=['POST'])
def prodwine():
    admin = Admin()
    admin.add_relation_wine(request.get_json()["product_name"], request.get_json()["wine_name"])
    return "Added", 201


@app.route('/prodspice/', methods=['POST'])
def prodspice():
    admin = Admin()
    admin.add_relation_spice(request.get_json()["product_name"], request.get_json()["spice_name"])
    return "Added", 201
