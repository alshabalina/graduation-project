import requests

from tkinter import Button, Entry, Label, mainloop


def get_wines(product):
    response = requests.post("http://localhost:5000/getwines/", json={"product_name": product})
    label2["text"] = ', '.join(response.json()["wines"])

def get_spices(product):
    response = requests.post("http://localhost:5000/getspices/", json={"product_name": product})
    label2["text"] = ', '.join(response.json()["spices"])

entry = Entry()
label1 = Label(text = "Нажимая на кнопку, вы можете подобрать к блюду специи или вино")
button1 = Button(text = "Нажми меня, чтобы подобрать вино", command = lambda: get_wines(entry.get()))
button2 = Button(text = "Нажми меня, чтобы подобрать специи", command = lambda: get_spices(entry.get()))
label2 = Label()
entry.pack()
button1.pack()
button2.pack()
label2.pack()
mainloop()