from projectdb import GUI

from flask import Flask, request
from flask.json import jsonify


app = Flask(__name__)

@app.route('/getwines/', methods=["POST"])
def get_wines():
    gui = GUI()
    result = gui.get_wines(request.get_json()["product_name"])
    return jsonify({"wines": result})


@app.route('/getspices/', methods = ["POST"])
def get_spices():
    gui = GUI()
    result = gui.get_spices(request.get_json()["product_name"])
    return jsonify({"spices": result})
