import sqlalchemy as sa
from datetime import datetime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

Base = declarative_base()

product_wine = sa.Table("product_wine", Base.metadata,
    sa.Column("product_id", sa.Integer, sa.ForeignKey("product.id")),
    sa.Column("wine_id", sa.Integer, sa.ForeignKey("wine.id")),
    )

product_spice = sa.Table("product_spice", Base.metadata,
    sa.Column("product_id", sa.Integer, sa.ForeignKey("product.id")),
    sa.Column("spice_id", sa.Integer, sa.ForeignKey("spice.id")),
    )

class Common:
    id = sa.Column(sa.Integer, primary_key=True)


class Product(Common, Base):
    __tablename__ = "product"
    name = sa.Column(sa.String(256), nullable = False, unique = True)
    wines = relationship("Wine", secondary = product_wine, back_populates = "products")
    spices = relationship("Spice", secondary = product_spice, back_populates = "products")


class Wine(Common, Base):
    __tablename__ = "wine"
    name = sa.Column(sa.String(256), nullable = False)
    products = relationship("Product", secondary = product_wine, back_populates = "wines")


class Spice(Common, Base):
    __tablename__ = "spice"
    name = sa.Column(sa.String(256), nullable = False)
    products = relationship("Product", secondary = product_spice, back_populates = "spices")


class Role:
    def __init__(self):
        self.engine = sa.create_engine("sqlite:///database2.sqlite")
        Session = sessionmaker()
        Session.configure(bind=self.engine)
        self.session = Session()

        Base.metadata.create_all(self.engine)

    def get_wines(self, name):
        return [name for (name,) in self.session.query(Wine.name).filter(Wine.products.any(Product.name == name)).all()]

    def get_spices(self, name):
        return [name for (name,) in self.session.query(Spice.name).filter(Spice.products.any(Product.name == name)).all()]


class Alice(Role):
    pass


class Admin(Role):
    def add_product(self, name):
        product = Product(name = name)
        self.session.add(product)
        self.session.commit()

    def add_wine(self, name):
        wine = Wine(name = name)
        self.session.add(wine)
        self.session.commit()

    def add_spice(self, name):
        spice = Spice(name = name)
        self.session.add(spice)
        self.session.commit()

    def add_relation_wine(self, product_name, wine_name):
        product = self.session.query(Product).filter(Product.name == product_name).first()
        wine = self.session.query(Wine).filter(Wine.name == wine_name).first()
        product.wines.append(wine)
        self.session.commit()

    def add_relation_spice(self, product_name, spice_name):
        product = self.session.query(Product).filter(Product.name == product_name).first()
        spice = self.session.query(Spice).filter(Spice.name == spice_name).first()
        product.spices.append(spice)
        self.session.commit()
    
class GUI(Role):
        pass
