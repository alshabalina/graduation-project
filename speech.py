from flask import Flask, request
from flask.json import jsonify

from projectdb import Alice


app = Flask(__name__)

@app.route("/", methods=["POST"])
def main():
    response = {
        "version": request.json["version"],
        "session": request.json["session"],
        "response": {
            "end_session": False
        }
    }

    handle_dialog(request.json, response)

    return jsonify(response)

def handle_dialog(req, res):
    user_id = req["session"]["user_id"]

    if req["session"]["new"]:
        res["response"]["text"] = "Привет! Используя этот навык, ты можешь научиться классно подбирать специи и вина к своим блюдам. Просто назови продукт, например, \"Индейка\" или \"Томат\" и я назову тебе подходящие специи и вина. "
        return
    
    product = req["request"]["original_utterance"].lower()

    alice = Alice()
    wines = ', '.join(alice.get_wines(product))
    spices =', '.join(alice.get_spices(product))
    
    res["response"]["text"] = f"Подходящие вина к {product} : {wines}, а подходящие специи к {product} - это {spices}"

